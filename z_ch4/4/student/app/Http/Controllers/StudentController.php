<?php
namespace App\Http\Controllers;
use DB;
class StudentController extends  Controller
{
    public function index()
    {   
	    $students=DB::table('student')->paginate(1);
        //跳转转到视图student文件夹下的index.blade.php
	    return view('student.index',[
	        'students'=>$students
	    ]);
    }
}