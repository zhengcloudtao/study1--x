<!-- 
(8) 引入css样式，pulic下的css文件夹中的bootstrap.min.css
(9) CSRF保护使用方法
(10) 显示错误提示信息
-->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>表单验证</title>
		<link rel="stylesheet" href="./css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
		
		<form class="mt-sm-5" method="post" action="{{ url('students/create') }}">
		    {{csrf_token()}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />

		    <div class="form-group row">
		        <label for="name" class="col-sm-3 control-label">姓名</label>
		 
		        <div class="col-sm-6">
		            <input type="text" value="{{ old('Student')['name'] }}" name="Student[name]" class="form-control" id="name" placeholder="请输入学生姓名">
		        </div>
{{--				@if($errors->has('Student.name'))--}}
		        <div class="col-sm-3">
		            <p class="form-control-static text-danger">{{$errors->first('Student.name')}}</p>
		        </div>
{{--				@endif--}}
		    </div>
		    <div class="form-group row">
		        <label for="age" class="col-sm-3 control-label">年龄</label>
		 
		        <div class="col-sm-6">
		            <input type="text" value="{{ old('Student')['age'] }}" name="Student[age]" class="form-control" id="age" placeholder="请输入学生年龄">
		        </div>
{{--				@if(count($errors) > 0)--}}
		        <div class="col-sm-3">
		            <p class="form-control-static text-danger">{{ $errors->first('Student.age') }}</p>
		        </div>
{{--				@endif--}}
		    </div>
		    <div class="form-group row">
		        <label class="col-sm-3 control-label">性别</label>
		 
		        <div class="col-sm-6">
		            <label class="radio-inline">
		                <input type="radio" name="Student[sex]" {{ (isset(old('Student')['sex']) && old('Student')['sex'] == '2') ? 'checked' : '' }} value="2"> 未知
		            </label>
		            <label class="radio-inline">
		                <input type="radio" name="Student[sex]" {{ (isset(old('Student')['sex']) && old('Student')['sex'] == '1') ? 'checked' : '' }} value="1"> 男
		            </label>
		            <label class="radio-inline">
		                <input type="radio" name="Student[sex]" {{ (isset(old('Student')['sex']) && old('Student')['sex'] == '0') ? 'checked' : '' }} value="0"> 女
		            </label>
		        </div>
{{--				@if(count($errors) > 0)--}}
		        <div class="col-sm-3">
		            <p class="form-control-static text-danger">{{ $errors->first('Student.sex') }}</p>
		        </div>
{{--				@endif--}}

		    </div>
		    <div class="form-group row">
		        <div class="offset-3 col-sm-9">
		            <button type="submit" class="btn btn-primary">提交</button>
		        </div>
		    </div>

			@if(!empty(session('success')))
				<div class="alert alert-success" role="alert">
					　　　　{{session('success')}}
				</div>
			@endif
		</form>
		</div>
	</body>
</html>
