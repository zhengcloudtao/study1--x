<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
(1) 表单提交路劲，根据上下文，填写路由
*/
Route::get('/', "HomeController@index");
Route::post('/students/create',"StudentsController@create");