<?php
//Route::get('/', function () {
//    return "view('welcome')";
//});

// 后台首页
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    // 后台商品管理模块
    Route::resource('goodss', 'GoodsController');
    Route::get('news_ajax', "Newscontroller@aajax");
    Route::get('login','LoginController@login');
});

