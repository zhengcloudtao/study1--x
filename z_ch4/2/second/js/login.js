$(function(){
	$(".title li").click(function(){
//给点击的元素添加类，将其他元素的cur类移除
		$(this).addClass('cur').siblings().removeClass('cur');
		$(".form form").eq($(this).index()).css("display","block").siblings.css("display","none");
	})

	$(".loginBtn").click(function(){
//获取表单中的值
		var username=$(".login .loginUsername").val();
		var password=$(".login .loginPwd").val();
		$.ajax({
			url: './login.php',
			type: 'post',
			dataType: 'json',
			data: {
				username: username,
				password: password
			}
		})
		.done(function(data) {
			if(data.code==1){
           //登录成功加载首页
				window.location.href="./index.html";
			}else{
           //弹出后台返回的message信息
				alert(data.message);
			}
		})
		
	})

	$(".registerBtn").click(function(){
		var username=$(".register .registerUsername").val();
		var password=$(".register .registerPwd").val();
		var passwordOk=$(".register .registerPwdOk").val();
       //判断两次密码输入是否一致
		if(password==passwordOk){
			$.ajax({
				url: './register.php',
				type: 'post',
				dataType: 'json',
			    data: {
					username: username,
					password: password
				}
			})
			.done(function(data) {
				if(data.code==1){
				//弹出后台返回的message信息
				    alert(data.message);
					window.location_="./login.html";
				}else{
				//弹出后台返回的message信息
				   alert(data.message);
				}
			})
		}else{
			alert("密码和确认密码不一致");
		}	
	})
})