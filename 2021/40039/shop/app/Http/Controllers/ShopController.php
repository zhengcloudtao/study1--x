<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//引入DB类
use DB;//第（3）空

class ShopController extends Controller  //第（4）空
{
    /**
     * 商品管理平台首页
     *
     */
    public function index()
    {
    	//获取商品列表
    	$shopList = DB::table('t_goods')->get();//第（5）空
    	return view('index', ['shopList' => $shopList]);//第（6）空
    }

    /**
     * 显示创建商品表单页.
     *
     */
    public function create()
    {
        return view('page.add');
    }

    /**
     * 保存新增的商品信息
     *
     */
    public function save(Request $request)				//第（7）空
    {
    	//接收新增商品信息
        $code = $request->post("code");
    	$name = $request->post("name");
    	$type = $request->post("type");
    	$price = $request->post("price");
    	$number = $request->post("number");
    	//判断商品编号是否已存在
    	$goods = DB::table('t_goods')->where('code',$code)->first();	//第（8）空
    	if ($goods) {
    		return view('page.add',['message'=>'此商品已存在！']);	//第（9）空
    	}

    	//保存新增的商品信息
    	$data = array(
    		'code' => $code,
    		'name' => $name,
    		'type' => $type,
    		'price' => $price,
        	'number' => $number,
    	);
    	$result = DB::table('t_goods')->insert($data);
        return redirect('/'); //第（10）空
    }

    /**
     * 编辑商品信息
     *
     */
    public function edit($id)
    {
    	//获取指定的商品信息
        $goods = DB::table('t_goods')->where('id',$id)->first();		//第（8）空
    	return view('page.change', ['goods' => $goods]);
    }

    /**
     * 保存更新的商品信息
     *
     */
    public function update(Request $request, $id)			//第（7）空
    {
    	//接收更新商品信息
        $code = $request->post("code");
    	$name = $request->post("name");
    	$type = $request->post("type");
    	$price = $request->post("price");
    	$number = $request->post("number");

    	//更新指定的商品信息
    	$data = array(
    		'code' => $code,
    		'name' => $name,
    		'type' => $type,
    		'price' => $price,
        	'number' => $number,
    	);
    	$result = DB::table('t_goods')->where('id', $id)->update($data);;
        return redirect('/');//第（10）空
    }

    /**
     * 删除指定资源
     *
     */
    public function delete($id)
    {
    	$result = DB::table('t_goods')->where('id', $id)->delete();
    	//删除成功，跳转至首页
        return redirect('/');//第（10）空
    }
}
