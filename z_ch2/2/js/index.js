$(function(){
	var num=1;//当前页数
var pageCount=7;//每页显示的数量
var pageNum=0;//总页数
	function ajaxDate(num){
		$.ajax({
			url: './adminList.php',
			type: 'get',
			dataType: 'json',
			data: {
				num: num,pageCount:pageCount//传递的参数
			},
			success:function(res){
				pageNum=Math.ceil(res.count/pageCount);//请求的总页数
				res.list.forEach(function(data){
					var str="<li>"+
							"<h2><a href=''>"+data.title+"</a></h2>"+
							"<span>"+ data.time+"</span>"+
							"</li>";
					$(".list ul").append(str);//将字符串追加到指定的位置
				})
			}
		})
	}
	ajaxDate(num);

	$(window).scroll(function(){
		var sTop=$(this).scrollTop();//获取滚动条卷进去的距离，用jquery
		var sHeight=$(this).height();//获取可视区域的高度，用jquery
		var bodyH=$(document).height();//获取页面所有内容的高度，用jquery
		if(sTop+sHeight>bodyH-1){//请写出判断条件
			num++;
			if(num<=pageNum){//请写出判断条件
				console.log(num);
				ajaxDate(num);//调用函数请求加载数据
			}
		}
	})
	
})