-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2020-08-23 10:41:21
-- 服务器版本： 10.1.37-MariaDB
-- PHP 版本： 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `article`
--

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` mediumtext NOT NULL,
  `time` datetime NOT NULL,
  `des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `time`, `des`) VALUES
(1, '世界上仅存的三条蛟龙，你见过那条？', '世界上有人拍到神仙，世界上仅存三条蛟龙再现。近日有人声称拍到的神仙，在网络上引起了轰动，当专家对传说中的神仙进行研究后，专家怀疑人们拍到的不是神仙，而是世界上仅存三条蛟龙再现，当真相被证实时，世人都被惊呆了，龙真的存在?\r\n\r\n  世界上有人拍到神仙 世界上仅存三条蛟龙再现\r\n\r\n  这世界上到底有没有神仙啊,关于世界上有没有神仙的说法一直争论不止,常听老人家说看到过神仙,也有说世界上有人拍到神仙。 如下据说是某人某天在某飞机上拍到的神仙照片。\r\n\r\n  世界上仅存的三条蛟龙真的存在吗?真相在这里。近日世界上仅存三条蛟龙再现的消息震惊世界，但是蛟龙是真的存在的吗?科学家们一直在寻找，近日终于找到大难了，专家在传说中冰封真龙的昆仑山找到了真相，真龙真相被揭开。', '2019-04-10 09:09:10', '世界上有人拍到神仙，世界上仅存三条蛟龙再现。近日有人声称拍到的神仙，在网络上引起了轰动，当专家对传说中的神仙进行研究后，专家怀疑人们拍到的不是神仙，而是世界上仅存三条蛟龙再现，当真相被证实时，世人都被惊呆了，龙真的存在?'),
(2, '最神秘的海洋世界', '<p>人类有史记载以来，海洋上发生的神秘事件一直是无法解答的谜团。</p>\r\n\r\n<p>一提起“百慕大三角”，大家就能想到那一连串的飞机和轮船的神秘失踪事件。</p>\r\n\r\n<p>所谓百慕大三角是指北起百慕大群岛，南到波多黎各，西至美国佛罗里达州这样一片三角形海域，面积约一百万平方公里。由于这一片海面失踪事件叠起，世人便称它为“地球的黑洞”、“魔鬼三角”。 在这个地区，已有数以百计的船只和飞机失事，数以千计的人在此丧生。</p>\r\n<p>据说从1880到1976年间，在这片海域约有158次失踪事件，其中大多是发生在1949年以来的30年间，曾发生失踪97次，至少有2000人在此丧生或失踪。这些奇怪神秘的失踪事件，主要是在西大西洋的一片叫“马尾藻海”地区，为北纬20°-40°、西经35°-75°之间的宽广水域。这儿是世界著名的墨西哥暖流以每昼夜120-190千米，且多漩涡、台风和龙卷风。不仅如此，这儿海深达4000-5000米，有波多黎各海沟，深7000米以上，最深达9218米。</p> ', '2019-12-10 12:16:06', '人类有史记载以来，海洋上发生的神秘事件一直是无法解答的谜团。一提起“百慕大三角”，大家就能想到那一连串的飞机和轮船的神秘失踪事件。');

--
-- 转储表的索引
--

--
-- 表的索引 `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
