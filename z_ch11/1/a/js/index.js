/*
(16) 滚动事件
(17) 滚动条距顶部距离
(18) 当前窗口高度
(19) 当前页面高度
(20) 计算总页面
(21) 遍历数据
*/ 
var num=1;
var pageNum=4;
var loading=false;
$(function(){
	getData(num,pageNum);
	$(window).scroll(function () {
		/*滚动条距顶部距离*/
		var scrollTop = parseFloat($(window).scrollTop());
		/*当前窗口高度*/
		var scrollHeight = $(window).height();
		/*当前页面高度*/
		var windowHeight = $(document).height();
		console.log(scrollTop + scrollHeight,windowHeight-10);
		if (scrollTop + scrollHeight >= windowHeight-10 && !loading) {
			num++;
			loading=true;
			getData(num,pageNum);
		}
		
	});
})



function getData(num,pageNum){
	$.ajax({
		type:"get",
		url:"list.php",
		data:{
			"page":num,
			"pageNum":pageNum
		},
		dataType:"json",
		success:function(res){
			pageCount=Math.ceil(res.count/pageNum);
			if(num>pageCount){
				var str="数据已经加载完毕";
				$("ul").after("<div style='text-align:center'>"+str+"</div>");
			}else{
				res.list.forEach(function(item){
					var str="<li>"+
						"<h2>"+ item.title +"</h2>"+
						"<p>"+ item.body +"</p>"+
						"<time>"+ item.date +"</time>"+
					"</li>";
					$("ul").append(str);
				});
				loading=false;
			}
		}
	})
}