<?php
$sql = "select empno,ename,deptno from emp where job = 'clerk'";		//根据传来的id查询marks表对应数据
$mysqli = new MySQLi('127.0.0.1','study','123456','study',3306);
$mysqli->set_charset('utf8');
$rs = $mysqli->query($sql);			//执行查询sql语句
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '找出从事clerk工作的员工的编号、姓名、部门号<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";

//

$sql="select * from emp where deptno = 10 and job='MANAGER' or deptno = 20 and job = 'CLERK'";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '找出10部门的经理、20部门的职员的员工信息(工作为"MANAGER"的是经理，工作为"CLERK"的是职员)<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";

//

$sql="select * from emp where comm>0";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '找出获得奖金的员工的工作<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";


//

$sql="select * from emp where length(ename) = 6";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '找到名字长度为6个字符的员工信息<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";


//

$sql="select * from emp where ename not like '%R%'";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '名字中不包含R字符的员工信息<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";

//

$sql="select deptno ,min(sal) sal from emp group by deptno";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '返回部门号及其本部门的最低工资<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";


//

$sql="select ename, sal * 12 as ySalary from emp group by ySalary desc ";
$rs=$mysqli->query($sql);
$arr = [];
while($row = $rs->fetch_object()){
    $arr[]=$row;
}
$jsonStr =json_encode($arr);	 //$arr转为json格式
echo '<br><br>';
echo '计算出员工的年薪，并且以年薪降序排序<br>';
echo 'sql:'.$sql."<br>";
echo "结果:".$jsonStr."<br>";
?>
