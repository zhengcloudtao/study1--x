<?php
/*
(5) 用pdo连接数据库
(6) 写出sql语句：查询newlist表中的所有数据
(7) 执行sql语句，此方法可以直接遍历返回的结果集
(8) 获取结果集中的所有行
(9) 获取每页显示的数量
(10)计算每页开始查找的数据的索引值
(11) 补全sql语句，根据id排序
(12) 补全sql语句，id降序排序
(13) 补全sql语句,限制查询结果返回的数量
(14) 将结果转成json数据
(15) 关闭数据库
*/
# 链接数据库
$servername="localhost";
$dbusername="study";
$dbpassword='123456';
$dbname="study";

try{
	$con=new PDO("mysql:dbname=$dbname;host=$servername",$dbusername,$dbpassword);
	# 设置编码
	$con->query("set names 'utf8'");
	
	# 数据的总数量
	$sqlTotal="select * from newslist";
	$st=$con->query($sqlTotal);
	$resultT=$st->fetchAll();//获取结果集中的所有行
	$count=count($resultT);
	
	# 当前的第几页
	if(isset($_GET['page'])){
		$num=$_GET['page'];
	}else{
		$num=1;
	}
	# 每页显示的数量
	$pageNum=$_GET['pageNum'];
	
	# 每页开始查询的数据
	$start=($num-1)*$pageNum;
	
	$sql="select * from newslist order by id desc limit $start,$pageNum";
	$result=$con->prepare($sql);
	$result->execute();
	$res=array(
		"count"=>$count,
		"list"=>$result->fetchAll()
	);
	echo json_encode($res);
}catch(Exception $e){
	echo "链接失败!".$e->getMessage();
	echo "错误代码：".$con->errorCode();
}
//mysqli_close($con);
# 关闭数据库
$con=null;