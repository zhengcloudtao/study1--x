-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2020-09-15 12:32:48
-- 服务器版本： 10.1.37-MariaDB
-- PHP 版本： 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `news`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `date`) VALUES
(1, 'admin', '123456', '2020-08-09 02:25:17'),
(2, 'news', '123456', '2020-08-09 07:22:07');

-- --------------------------------------------------------

--
-- 表的结构 `newslist`
--

CREATE TABLE `newslist` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `thumb` varchar(1000) NOT NULL,
  `body` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `newslist`
--

INSERT INTO `newslist` (`id`, `title`, `thumb`, `body`, `author`, `date`) VALUES
(17, '中国未来的8大赚钱机会，最好的时代才刚开始！', 'localhost:8080/test2/public/uploads/20200814/5f365a64a9e7b.png', '以2019年为元年，中国的商业正步入万象更新的阶段，有大破才有大立，建设与破坏必定同在', 'admin', '2020-08-14 09:33:24'),
(18, '互联网思维教你50平米餐厅年入100万', 'localhost:8080/test2/public/uploads/20200814/5f365a73eb1ce.jpg', '一家50平米的小餐厅，年收入120万，很多人可能会怀疑是不是夸大了收益，同样是餐厅为什么别人可以年入百万，看了下面介绍怎么利用互联网思维做餐饮你就会相信年入百万你也可以。', 'admin', '2020-08-14 09:33:39'),
(25, '58同城退市自救，前浪神奇不再，拆分自身能有用吗？', '', '原创 江瀚视野观察 江瀚视野观察 收录于话题#58同城1#天鹅到家1#姚劲', 'admin', '2020-08-16 03:42:11'),
(26, '直销行业有多少人赚钱的？不多，但你可以！', '', '从大环境来看，互联网+直销是下一个风口，直销行业大有可为。“互联网+直销”这种以人为销售通路的模式更容易产生信任和情感，从而更好地影响人的消费和决策，也就是直销模式的优化升级，更便于人人参与，因为顾客产品用的好利用互联网社交平台很自然的就可以分享。互联网+直销势不可挡，你如何把握当下？你可以输在起跑线，但绝不可以输在转折点！', '', '2020-09-15 10:29:32'),
(27, '餐厅一元一只鸡，老板每个月还能多拿15万', '', '在当今这个时代，互联网思想渗透在每个人的生活当中，无论你是一个在马路上做清洁的环卫工人，还是在世界各地参加各种国际会议的商业大佬，无一不将互联网当作一种或者消遣，或者创造的力量，通过这样的力量，让自己的生意或者生活，更加充满多元化的色彩', '', '2020-09-15 10:29:32'),
(28, '什么是互联网思维？怎么通过互联网思维赚钱？', '', '一个传统餐饮老板看到生意不好，他就会想是菜不好吃，还是没打广告，不断去试或者观察。而互联网思维就会想，我要看经营数据，每天进店客户多少，客单价多少，客户的评价怎么样。而且在开餐饮店的时候的想法也不一样，互联网思维的人，会想到用软件系统经营，机器人炒菜等', '', '2020-09-15 10:30:07'),
(29, '滴滴劲敌15个月连续盈利，积累1.8亿用户，已经崛起为行业第二', '', '近来，在出行领域这片新蓝海之上可以说硝烟再起，当然了并不是疯狂“烧钱”，首先来说滴滴最近的动作就颇为频繁，目前滴滴已经拆出了多个子品牌，似乎正在对整个市场进行细分，其中包括青菜拼车、花小猪打车、快的新出租等。另一边高德地图也宣布加入到这场硝烟之中，对外正式宣布要接入多家出租车公司，并且目前目前已经接入了3万余辆巡游出租车，除了这两家之外，还有一个滴滴劲敌也在进行布局！', '', '2020-09-15 10:30:07'),
(30, '成功的人，往往都读这“6”种书', '', '在这个信息爆炸、日新月异的时代当中，唯有看书阅读是大多数人都能接受的学习方式，以及提升自我的方式。要知道：人生就像逆水行舟，不想着进步，就意味着我们正在倒退；不想着学习补充，就意我们正在落后淘汰。', '', '2020-09-15 10:30:54'),
(31, '互联网+搭建智慧接种门诊', '', '健康意识不断提升，疫苗成为不少人呵护健康的选择。然而，疫苗信息和认知哪里获取、接种如何流程便捷？第二届进博会上，赛诺菲巴斯德与多家机构搭成战略性合作，携手共创智慧化、规范化疫苗接种体验。', '', '2020-09-15 10:30:54'),
(32, '要想异地投保能够得到理赔', '', '原则上是不允许保险公司跨省经营的，在符合规定的前提下，可以通过电话或者互联网的方式来进行投保。\r\n\r\n销售区域限制主要是出于监管的考虑，因为不同公司的公司规模、经营水平、盈利能力不同，为了方便进行监管，所有才会对销售区域进行要求', '', '2020-09-15 10:31:58');

--
-- 转储表的索引
--

--
-- 表的索引 `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `newslist`
--
ALTER TABLE `newslist`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `newslist`
--
ALTER TABLE `newslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
