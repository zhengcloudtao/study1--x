<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*
(2) 继承Controller
(3) 用视图的方式跳转页面
*/
class HomeController extends Controller
{
	public function index(){
		return view("welcome");
	}
}
