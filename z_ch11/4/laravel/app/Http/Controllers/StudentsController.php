<?php
/*
(4) 引入自定义模型student
(5) 判断请求方式
(6) 验证规则，补全代码
(7) 将输入数据闪存并重定向至前一页
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\student;
use DB;
class StudentsController extends Controller
{
    public function create(Request $request){
		// 判断是POST请求，也就是提交表单时走这个区间
		if($request->method('POST'))
		{
			// 校验
			$this->validate($request, [
			    'Student.name' => 'required|min:2|max:20',
			    'Student.age' => 'required|integer',
			    'Student.sex' => 'required|integer',
			],[
			    'required' => ':attribute 为必填项',
			    'min' => ':attribute 长度不能小于2个字符',
			    'max' => ':attribute 长度不能大于20个字符',
			    'integer' => ':attribute 必须为数字',
			],[
			    'Student.name' => '用户名',
			    'Student.age' => '年龄',
			    'Student.sex' => '性别',
			]);
		}
		// 模型的添加方法
		
		$data=array(
			"sname"=>$request->input('Student.name'),
			"sage"=>$request->input('Student.age'),
			"ssex"=>$request->input('Student.sex')
		);
		$ret = student::insert($data);
		if($ret)
		{
		    return redirect('/')->with('success', '添加成功！');
		} else{
		    return redirect('student/create')->with('error', '添加失败！');
		}
	}
}
