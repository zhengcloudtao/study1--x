<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>天下奇闻</title>
		<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    </head>
    <body>
        <div class="main">
			<div class="article">
				
				<h2>{{ $data->title }}</h2>
				<time>时间：{{ $data->time }}</time>
				<section>
					{!! $data->body !!}
				</section>
			</div>
		</div>
    </body>
</html>
