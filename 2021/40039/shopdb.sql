/*
Navicat MySQL Data Transfer

Source Server         : xampp-mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : shopdb

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001
*/

drop database if exists shopdb;
create database shopdb;
use shopdb;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for `t_goods`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(16) DEFAULT NULL COMMENT '商品编号',
  `name` varchar(256) DEFAULT '' COMMENT '商品名称',
  `type` varchar(64) DEFAULT NULL COMMENT '商品种类',
  `price` float DEFAULT NULL COMMENT '商品价格',
  `number` int(11) DEFAULT 0 COMMENT '商品数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_goods
-- ----------------------------
INSERT INTO `t_goods` VALUES ('1', 'RY163361166', '厨房家庭塑料圆凳子创意收纳', '日用品', '18.75', '1137');
INSERT INTO `t_goods` VALUES ('2', 'RY111302075', '360°小夜灯', '日用品', '59.9', '31');
INSERT INTO `t_goods` VALUES ('3', 'SP107421121', '五谷磨房高纤魔芋粉椰子粉', '食品', '98', '12557');
INSERT INTO `t_goods` VALUES ('4', 'SP057100076', '桂香村苏州特产奶糕米糕手工零添加', '食品', '52', '18228');
INSERT INTO `t_goods` VALUES ('5', 'FZ186571022', '高领德绒打底衫女2021新款', '服装', '149', '5985339');
INSERT INTO `t_goods` VALUES ('6', 'GJ439283782', '电磨机小型手持打磨机', '工具', '316.5', '301807');


