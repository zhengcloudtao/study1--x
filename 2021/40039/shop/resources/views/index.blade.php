<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>商品管理平台</title>
		<link rel="stylesheet" type="text/css" href="{{URL::asset('css/index.css')}}" /><!-- 第（11）空 -->
	</head>
	<body>
		<div id="con">
			<h5 class="tit">商品管理平台</h5>
			<div class="table_con">
				<table>
					<thead>
						<tr>
							<th>编号</th>
							<th>名称</th>
							<th>分类</th>
							<th>价格</th>
							<th>数量</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						@foreach($shopList as $v)		<!-- 第（12）空 -->
						<tr>
							<td>{{$v->code}}</td>
							<td>{{$v->name}}</td>
							<td>{{$v->type}}</td>
							<td>{{$v->price}}</td>
							<td>{{$v->number}}</td>
							<td>
								<div>
									<a href="./update/{{$v->id}}">
										<button type="button" class="change_btn">
											修改
										</button>
									</a>
									<a href="./del/{{$v->id}}">
										<button type="button" class="del_btn">
											删除
										</button>
									</a>
								</div>
							</td>
						</tr>
						@endforeach               <!-- 第（13）空 -->
						<tr>
							<td colspan="6">
								<a href="./add">
									<button type="button" class="add_btn">
										添加
									</button>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	</body>
</html>
