<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//商品管理平台首页路由
Route::get('/', 'ShopController@index');
//商品添加路由
Route::get('/add',  "ShopController@create");
Route::post('/add',  "ShopController@save");	//第（1）空

//商品修改路由
Route::get('/update/{id}',  "ShopController@edit");
Route::post('/update/{id}',  "ShopController@update");//第（2）空

//商品删除路由
Route::get('/del/{id}',  "ShopController@delete");
