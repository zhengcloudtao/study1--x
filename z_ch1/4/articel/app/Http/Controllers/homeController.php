<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class homeController extends Controller
{
    public function index(Request $request){
		$re=DB::table('news')->get();
		return view('welcome')->with("data",$re);
	}
}
