<!DOCTYPE html>
<html lang="zh">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>商品添加</title>
		<link rel="stylesheet" type="text/css" href="{{URL::asset('css/index.css')}}" />
	</head>

	<body>
		<div id="con">
			<h5 class="tit">商品添加</h5>
			<div class="table_con">
				<table>
					<thead>
						<tr>
							<th>标题</th>
							<th>内容</th>
						</tr>
					</thead>
					<tbody>
						<form action="./add" method="post">
							<!-- csrf令牌生成 -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />     <!-- 第（14）空-->

							<tr>
								<td>编号：</td>
								<td>
									<input class="input" type="text" name="code" value="" required="required"/>
								</td>
							</tr>
							<tr>
								<td>名称：</td>
								<td>
									<input class="input" type="text" name="name" value="" required="required"/>
								</td>
							</tr>
							<tr>
								<td>分类：</td>
								<td>
									<input class="input" type="text" name="type" value="" required="required"/>
								</td>
							</tr>
							<tr>
								<td>价格（元）：</td>
								<td>
									<input class="input" type="number" step="0.01" name="price" value="" required="required"/>
								</td>
							</tr>
							<tr>
								<td>数量：</td>
								<td>
									<input class="input" type="number" name="number" value="" required="required"/>
								</td>
							</tr>
							@if(isset($message))		<!-- 第（15）空 -->
							<tr>
								<td colspan="2">
									<span class="text-warning">{{$message}}</span>
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<button type="submit" class="add_btn">添加</button>
								</td>
							</tr>
						</form>

					</tbody>
				</table>
			</div>
	</body>

</html>
