<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>天下奇闻</title>
		<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    </head>
    <body>
        <div class="main">
			<h1>天下奇闻</h1>
			<ul>
				@foreach($data as $v)
				<li>
					<a href="{{ url('show') }}/{{ $v->id }}">
					<h2>{{ $v->title }}</h2>
					<p>{{ $v->des }}</p>
					<time>{{$v->time}}</time>
					</a>
				</li>
			@endforeach
			</ul>
		</div>
    </body>
</html>
