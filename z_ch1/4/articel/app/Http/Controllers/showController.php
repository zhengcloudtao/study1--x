<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class showController extends Controller
{
    public function index($id){
		$re=DB::table('news')->where("id",$id)->first();
		return view("show")->with('data',$re);
	}
}
