运行效果:
http://sc.cloudduolc.cn

nginx配置

    location / {
       try_files $uri  $uri/ /index.php?$query_string;
    }
    
    location ^~ /articel/ {    
          
          try_files $uri $uri/ /z_ch1/4/articel/public/index.php?$query_string;
    }
    location ~ /yiwularavel/ {    
         
          try_files $uri $uri/ 
          /z_ch2/4/yiwularavel/public/index.php?$query_string;
    }
    location ~ /blog/ {    
        
          try_files $uri $uri/ /z_ch3/4/blog/public/index.php?$query_string;
    }
    location ~ /student/ {    
         
          try_files $uri $uri/ /z_ch4/4/student/public/index.php?$query_string;
    }
     location ~ /feiyu/ {    
          
          try_files $uri $uri/ /z_ch5/3/feiyu/public/index.php?$query_string;
    }